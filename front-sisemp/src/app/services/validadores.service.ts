import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { promise } from 'selenium-webdriver';
import { EmpleadoModel } from '../models/empleado.model';
import { EmpleadoService } from './empleado.service';

interface validacion {
  [s: string]: boolean
}

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  constructor(
    private empleadoService: EmpleadoService
  ) { }

  existeUser(control: FormControl): Promise<validacion> | Observable<validacion> {

    const user = control.value.toLowerCase();

    if(!user){
      return Promise.resolve(null);
    }

    return new Promise((resolve, reject) => {

      this.empleadoService.findByUser(user)
      .subscribe((resp: EmpleadoModel[]) => {   
        const info = resp;
        info.forEach(dat => {
          if (user === dat.username) {
            resolve({ existe: true });
          } else {
            resolve(null);
          }
        });
      });

    });

  }
}
