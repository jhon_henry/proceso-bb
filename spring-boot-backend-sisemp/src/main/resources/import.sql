/*EMPLOYEES*/
INSERT INTO employees (fullname,function,salary,username,boss_id) VALUES ('John Doe','cashier',1000000,'johndoe',null);
INSERT INTO employees (fullname,function,salary,username,boss_id) VALUES ('Edwin Doe','cashier',500000,'edwindoe',1);
INSERT INTO employees (fullname,function,salary,username,boss_id) VALUES ('Pepito Salazar','cashier',500000,'pepitosalazar',1);
INSERT INTO employees (fullname,function,salary,username,boss_id) VALUES ('Edwin Cardenas','cashier',500000,'edwincardenas',1);
INSERT INTO employees (fullname,function,salary,username,boss_id) VALUES ('John Cardenas','cashier',500000,'johncardenas',1);
INSERT INTO employees (fullname,function,salary,username,boss_id) VALUES ('Henry Doe','cashier',500000,'Henrydoe',1);
INSERT INTO employees (fullname,function,salary,username,boss_id) VALUES ('Daniel Potter','cashier',500000,'danielpotter',1);
