export class EmpleadoModel {
    id: string;
    fullname: string;
    function: string;
    salary: string;
    username: string;
    boss: EmpleadoModel;

    constructor(){
        this.id = '';
        this.fullname = '';
        this.function = '';
        this.salary = '';
        this.username = '';
        
    }
}