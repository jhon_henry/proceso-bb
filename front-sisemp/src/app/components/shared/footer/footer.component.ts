import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styles: [
  ]
})
export class FooterComponent implements OnInit {

  fecha: Date = new Date();
  anio: number;

  constructor() { 
    this.anio = this.fecha.getFullYear();
  }

  ngOnInit(): void {
  }

}
