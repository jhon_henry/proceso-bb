package com.jhonmurillo.springboot.backend.emp.models.service;

import java.util.List;

import com.jhonmurillo.springboot.backend.emp.models.entity.Employee;

public interface IEmployeeService {
	
	public List<Employee> findAll();
	
	public Employee findOne(Long id);
	
	public List<Employee> findByUsername(String criteria);
	
	public void delete(Long id);
	
	public Employee save(Employee employee);
	
	public Employee updateEmployee(Long id, Employee employee);

}
