package com.jhonmurillo.springboot.backend.emp.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jhonmurillo.springboot.backend.emp.models.dao.IEmployeeDao;
import com.jhonmurillo.springboot.backend.emp.models.entity.Employee;

@Service
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	private IEmployeeDao employeeDao;

	@Override
	@Transactional(readOnly = true)
	public List<Employee> findAll() {
		return employeeDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Employee findOne(Long id) {
		return employeeDao.findById(id).orElse(null);
	}

	@Override
	public List<Employee> findByUsername(String criteria) {
		return employeeDao.findByUsername(criteria);
	}

	@Override
	public void delete(Long id) {
		employeeDao.deleteById(id);
	}

	@Override
	public Employee save(Employee employee) {
		return employeeDao.saveAndFlush(employee);
		
	}

	@Override
	public Employee updateEmployee(Long id, Employee employee) {
		return employeeDao.findById(id)
				.map(data -> {
					data.setFullname(employee.getFullname());
					data.setFunction(employee.getFunction());
					data.setSalary(employee.getSalary());
					data.setUsername(employee.getUsername());
					data.setBoss(employee.getBoss());
					return employeeDao.saveAndFlush(data);
				}).orElse(null);
	}
}
