package com.jhonmurillo.springboot.backend.emp.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jhonmurillo.springboot.backend.emp.models.entity.Employee;

public interface IEmployeeDao extends JpaRepository<Employee, Long> {
	
	List<Employee> findByUsername(String criteria);
	

}
