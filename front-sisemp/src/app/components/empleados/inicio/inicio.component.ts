import { Component, OnInit } from '@angular/core';
import { EmpleadoModel } from 'src/app/models/empleado.model';
import { EmpleadoService } from 'src/app/services/empleado.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styles: []
})
export class InicioComponent implements OnInit {

  empleados: EmpleadoModel[];

  constructor(
    private empleadoservice: EmpleadoService
  ) { }

  ngOnInit(): void {
    this.empleadoservice.findEmpleados().subscribe((resp: EmpleadoModel[]) => {
      this.empleados = resp;
      console.log(this.empleados);
    });
  }

  borrarEmpleado(empleado: EmpleadoModel, i: number) {

    Swal.fire({
      title: `¿Esta seguro?`,
      text: `Borrar ${empleado.fullname}?`,
      icon: 'warning',
      showConfirmButton: true,
      showCancelButton: true
    }).then( resp => {
      if( resp.value ){
        this.empleados.splice(i, 1);
        this.empleadoservice.deleteEmpleado(empleado.id).subscribe();
      }
    });

  }

}
