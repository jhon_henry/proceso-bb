package com.jhonmurillo.springboot.backend.emp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jhonmurillo.springboot.backend.emp.models.entity.Employee;
import com.jhonmurillo.springboot.backend.emp.models.service.IEmployeeService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/employees")
public class EmployeeRestController {

	@Autowired
	private IEmployeeService employeeService;

	@PostMapping("/create")
	public Employee createEmployee(@RequestBody Employee empleado) {
		System.out.println(empleado);
		if(empleado.getId() == null){
			
			return employeeService.save(empleado);
		}
		return null;
	}
	
	@GetMapping
	public List<Employee> index() {
		return employeeService.findAll();
	}
	
	@GetMapping("/{id}")
	public Employee findById(@PathVariable Long id) {
		return employeeService.findOne(id);
	}
	
	@GetMapping("/find/{criteria}")
	public List<Employee> findByUsername(@PathVariable String criteria) {
		return employeeService.findByUsername(criteria);
	}
	
	@DeleteMapping("/{id}")
	public void deleteEmployee(@PathVariable Long id) {
		employeeService.delete(id);
	}
	
	@PutMapping("/{id}")
	public Employee updateEmployee(@PathVariable Long id,@RequestBody Employee empleado) {
		return employeeService.updateEmployee(id, empleado);
				
	}

}
