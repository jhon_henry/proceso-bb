import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { EmpleadoModel } from '../models/empleado.model';

import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  private url: string = 'http://localhost:8080/employees';

  constructor(
    private http: HttpClient
  ) { }

  getInfo(respuesta: EmpleadoModel[]): EmpleadoModel[] {

    for (var i = 0; i < respuesta.length; i++) {
      let item: EmpleadoModel = respuesta[i];
      if (!item.boss) {
         respuesta[i].boss = new EmpleadoModel();
      }
    }
    return respuesta;
  }

  createEmpleado(empleado: EmpleadoModel){
    console.log(empleado);
    
    return this.http.post(`${ this.url }/create`,empleado).pipe(
      map((resp:EmpleadoModel)=>{
        console.log(resp);
        empleado = resp;
        return empleado;
      }));
    
  }

  updateEmpleado(empleado: EmpleadoModel){

    const empleadoTemp = {
      ...empleado
    };
    delete empleadoTemp.id;
    
    return this.http.put(`${ this.url }/${ empleado.id }`,empleadoTemp);
  }

  findByUser( user: string){    
    return this.http.get(`${ this.url }/find/${ user }`);
  }

  findEmpleados() {
    return this.http.get(`${ this.url }`).pipe(map((data:EmpleadoModel[]) => {
      return this.getInfo(data);
    }));
  }

  findEmpleadoById( id:string ){
    return this.http.get(`${ this.url }/${ id }`).pipe(map((data: EmpleadoModel) =>{

      if( !data.boss ){
        data.boss = new EmpleadoModel();
      }
      return data;
    }));
  }

  deleteEmpleado( id:string ){
    return this.http.delete(`${ this.url }/${id}`);
  }
}
