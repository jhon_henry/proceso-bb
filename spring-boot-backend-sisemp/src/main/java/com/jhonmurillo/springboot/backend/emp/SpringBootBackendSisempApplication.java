package com.jhonmurillo.springboot.backend.emp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBackendSisempApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBackendSisempApplication.class, args);
	}

}
