import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmpleadoModel } from 'src/app/models/empleado.model';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { ActivatedRoute } from "@angular/router";
import { ValidadoresService } from 'src/app/services/validadores.service';
import  Swal  from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styles: [
  ]
})
export class FormComponent implements OnInit {

  empleado = new EmpleadoModel();
  emp: EmpleadoModel[] = [];
  forma: FormGroup;

  constructor(
    private empleadoService: EmpleadoService,
    private validadoresService: ValidadoresService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.crearFormulario();
  }

  ngOnInit(): void {
    this.cargaFormulario();
  }

  get nombreNoValido() {
    return this.forma.get('fullname').invalid && this.forma.get('fullname').touched;
  }

  get userNoValido() {
    let resp: boolean = false;
    resp = this.forma.get('username').invalid && this.forma.get('username').touched
    return resp;
  }

  get userRepetido() {

    if (!this.forma.get('username').errors) {
      return false;
    } else if (this.forma.value.id) {
      return false;
    }

    return this.forma.get('username').errors.existe;
  }

  get funcionNoValido() {
    return this.forma.get('function').invalid && this.forma.get('function').touched;
  }

  get salarioNoValido() {
    return this.forma.get('salary').invalid && this.forma.get('salary').touched;
  }

  get jefeNoValido() {
    return this.forma.get('boss').invalid && this.forma.get('boss').touched;
  }

  crearFormulario() {
    this.forma = this.fb.group({
      id: [''],
      fullname: ['', Validators.required],
      username: ['', [Validators.required], this.validadoresService.existeUser.bind(this)],
      function: ['', Validators.required],
      salary: ['', Validators.required],
      boss: ['']
    });

  }

  cargaFormulario() {

    const id = this.route.snapshot.paramMap.get('id');
    this.empleadoService.findEmpleados().subscribe((resp: EmpleadoModel[]) => {
      this.emp = resp
      this.emp.unshift({
        id: '',
        fullname: '[ Seleccione Jefe ]',
        username: '',
        function: '',
        salary: '',
        boss: new EmpleadoModel()
      });
    });

    if (id !== 'create') {
      this.empleadoService.findEmpleadoById(id).subscribe((resp) => {

        this.forma.reset({
          id: resp.id,
          fullname: resp.fullname,
          username: resp.username,
          function: resp.function,
          salary: resp.salary,
          boss: (resp.boss.id) ? resp.boss.id : ''
        });
      });
    }

  }

  guardar() {

    if (this.forma.invalid && !this.forma.value.id) {
      return Object.values(this.forma.controls).forEach(control => {
        control.markAsTouched();
      });
    }

    Swal.fire({
      title: 'Procesando...',
      text: 'Guardando información',
      icon: 'info',
      allowOutsideClick: false
    });
    Swal.showLoading();


    this.empleado.id = this.forma.value.id;
    this.empleado.fullname = this.forma.value.fullname;
    this.empleado.username = this.forma.value.username;
    this.empleado.function = this.forma.value.function;
    this.empleado.salary = this.forma.value.salary;
    if (this.forma.value.boss != '') {
      this.empleadoService.findEmpleadoById(this.forma.value.boss).subscribe((resp: EmpleadoModel) => {
        this.empleado.boss = resp;

      });
    } else {
      this.empleado.boss = null;

    }

    this.ejecutaServiceGuardar(this.empleado).subscribe(resp =>{
      Swal.fire({
        title: this.empleado.fullname,
        text: 'finalizado',
        icon: 'success'
      });
      
    });
    window.history.back();

  }

  ejecutaServiceGuardar(empleado: EmpleadoModel):Observable<any> {
    let peticion: Observable<any>;
    if (!this.empleado.id) {
      peticion = this.empleadoService.createEmpleado(this.empleado);
    } else {
      peticion = this.empleadoService.updateEmpleado(this.empleado);
    }
    return peticion;
  }

}
