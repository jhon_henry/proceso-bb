import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from './components/empleados/form/form.component';
import { InicioComponent } from "./components/empleados/inicio/inicio.component";

const routes: Routes = [
  { path: 'empleados', component: InicioComponent },
  { path: 'empleados/:id', component: FormComponent },
  { path: '', pathMatch: 'full', redirectTo: 'empleados' },
  { path: '**', pathMatch: 'full', redirectTo: 'empleados' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
